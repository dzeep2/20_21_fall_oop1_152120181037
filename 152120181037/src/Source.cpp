#include<iostream>
#include<stdlib.h>
#include<fstream>
using namespace std;
int sumfunc(int arr[], int sizeofarray) ///sum function created. takes two parameter. 
{
	int sum = 0;/// integer sum assigned to zero.
	for (int i = 0; i < sizeofarray; i++) {
		sum = arr[i] + sum;///calculated sum of integers.
	}

	return sum;
}
int productfunc(int arr[], int size) { /// Product function created.
	int product = 1;
	for (int i = 0; i < size; i++) {
		product = arr[i] * product; /// Product calculated.
	}
	return product;
}
float averagefunc(int arr[], int size) {/// Average function created.
	float avg;
	int summ = 0;
	for (int i = 0; i < size; i++) {
		summ = arr[i] + summ;
	}

	avg = (float)(summ) / float(size);

	return avg;
}
int findSmallestElement(int arr[], int n) {
	int temp = arr[0];
	for (int i = 0; i < n; i++) {
		if (temp > arr[i]) {
			temp = arr[i];
		}
	}
	return temp;
}
int main() {
	int size;
	fstream dataFile;
	char filename[81] = "input.txt";


	dataFile.open(filename, ios::in);///file opened with read only.
	if (!dataFile)
	{
		cout << "The file " << filename << " was not opened.\n";
	}
	else
	{
		cout << "The file " << filename << " was opened successfully.\n";

	}

	dataFile >> size; ///size read from file. 
	int* array = new int[size];// dynamic array created.
	cout << endl;
	for (int i = 0; i < size; i++) {

		dataFile >> array[i];///integer added to dynamic array.
		if ((array[i] >= 'a' && array[i] <= 'z') || (array[i] >= 'A' && array[i] <= 'Z') || array[i] <= 0) {

			cout << "variables is not integer.";
			exit(0);
		}
	}

	if ((size >= 'a' && size <= 'z') || (size >= 'A' && size <= 'Z') || size <= 0) {

		cout << "size is not integer.";
		exit(0);
	}
	///functions calledand write functions outputs to console.

	cout << "Sum is " << sumfunc(array, size) << endl;
	cout << "Product is " << productfunc(array, size) << endl;
	cout << "Average is " << averagefunc(array, size) << endl;
	cout << "Smallest is " << findSmallestElement(array, size) << endl;
	delete[] array;
	return 0;
}