#include<bits/stdc++.h>

using namespace std;
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

int BoxesCreated,BoxesDestroyed;
class Box{
    int l,b,h;
    public:
        Box(){
            l=0;
            b=0;
            h=0;
            BoxesCreated++;
        }
        Box(int l,int b,int h){
            this->l=l;
            this->b=b;
            this->h=h;
            BoxesCreated++;
        }
        Box(Box &box){
            this->l=box.getLength();
            this->h=box.getHeight();
            this->b=box.getBreadth();
            BoxesCreated++;
        }
        ~Box(){
            BoxesDestroyed++;
        }
    
        int getLength(){
            return l;
        }
        int getBreadth(){
            return b;
        }
        int getHeight(){
            return h;
        }
        long long CalculateVolume(){
            long long volume=(long long)l*(long long)b*(long long)h;
            return volume; 
        }
        bool operator<(const Box& B){
            if(l<B.l){
                return true;
            }
            if(b<B.b && l==B.l){
                return true;
            }
            if(h<B.h && b==B.b && l==B.l){
                return true;
            }
            return false;
        }
        friend ostream &operator<<(ostream &out,Box B){
            out << B.getLength() << " " << B.getBreadth() << " " << B.getHeight() << " ";
            return out;
        }
    
};
